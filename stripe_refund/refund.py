import os
import datetime
import logging
import pathlib

from dotenv import load_dotenv
import stripe

#setup logging
stream_handler = logging.StreamHandler()
file_handler = logging.FileHandler("/mnt/log/refund.log")
# formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.basicConfig(handlers=[stream_handler, file_handler], level=logging.INFO, format='%(asctime)s|%(name)s|%(levelname)s|%(message)s')

stripe_logger = logging.getLogger("stripe").setLevel(logging.WARNING)
logger = logging.getLogger("refund")

def get_all_active_charges(created={}):
    charges = stripe.Charge.list(limit=100, created=created)
    active_charges = []
    while not charges.is_empty:
        active_charges.extend([charge for charge in charges if charge.status == "succeeded" and not charge.refunded])
        charges = charges.next_page()
    return active_charges

if __name__ == "__main__":
    #load the contents of .local into environment variables
    if not os.environ.get("STRIPE_API_KEY", None):
        load_dotenv(".local")

    try:
    # store the env variabel with the stripe api key in the stripe object
        stripe.api_key = os.environ["STRIPE_API_KEY"]
        stripe.log = "error"
    except Exception as e:
        logger.exception(e)


    #create the time window to search for charges
    gt_date = datetime.datetime.utcnow() - datetime.timedelta(hours=240)

    # create a "created" object to pass as a filter to the api call
    created = {
        "gt": gt_date
    }

    # retrieve charges from the stripe api
    charges = get_all_active_charges(created=created)
    logger.info(f"There are {len(charges)} to process")

    for charge in charges:
        try:
            charge.refund()
            logger.info(f"refund_complete for {charge.id}")
        except Exception as e:
            logger.error(f"Refund failed for {charge.id}")



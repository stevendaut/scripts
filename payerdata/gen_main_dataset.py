# Merge each sheet from the excel data in each file in rawdata and 
# fix the users 

import pathlib, logging, sys

import pandas as pd

import config.configuration as config


logger = logging.getLogger(__name__)
rawdata_dir = pathlib.Path('rawdata')
rawdata_dir.mkdir(exist_ok=True)
output_dir = pathlib.Path('maindata')
rawdata_dir.mkdir(exist_ok=True)


# Process step 1
# make a cleaned and ordered complete dataset that is complete and has
# an identifier for every patient as to where they come from.

# Columns added
# EXCEL_SHEET = the name of the sheet they came from, e.g. retrospective
# EXCEL_FILE = name of the excel file they came from.



# a fun list of dataframes to merge later.
dfs = []

for fn in rawdata_dir.iterdir():
    
    logger.info(f"Working on file {fn}")

    pdxl = pd.ExcelFile(fn)
    


    for sheet in pdxl.sheet_names:

        logger.info(f"Working on sheet name {sheet}")
        df = pdxl.parse(sheet, skiprows=4)

        logger.info(f"headers are {df.columns}")

        logger.info(f"Data frame raw is {df}")

        df[['LAST_NAME', 'FIRST_NAME']] = df.MBR_NAME.str.split(', ', expand=True)
        df["EXCEL_SHEET"] = sheet
        df["EXCEL_FILE"] = fn.stem
 
        logger.info(f"Parsed df is {df}")

        dfs.append(df)


df = pd.concat(dfs, ignore_index=True)
df.index.name = "UUID"
logger.info(f"Main dataframe is \n {df}")
# write it to our csv
output_file = output_dir / ("main_dataset.csv")
df.to_csv(output_file)

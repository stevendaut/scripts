# Like everything subsequent to our main data manipulation, we want to work from
# our maindata/main_dataset.csv 

import pathlib, logging, sys

import pandas as pd

import config.configuration as config


logger = logging.getLogger(__name__)

output_columns = [
    "LAST_NAME",
    "FIRST_NAME",
    "MBR_DOB",
    "MBR_SEX",
    "RAC_RACE_DESC",
    "MBR_ADDR1",
    "MBR_ADDR2",
    "MBR_CITY",
    "MBR_STATE",
    "MBR_ZIP_CODE",
    "MBR_PHONE"
]


main_dataset_csv = pathlib.Path('maindata/main_dataset.csv')

output_dir = pathlib.Path('neustar')


# Generate our dataframe, hoorah!
df = pd.read_csv(main_dataset_csv, index_col=0)

logger.info(f"This is our datafraome \n {df}")

# Filtration rules

# 1. get prospective people only.
df = df[ df["EXCEL_SHEET"] == "PROSPECTIVE" ]

logger.info(f"New Dataframe with only prospective customers \n {df}")


df = df[ df["EXCLUSION_REASON"].str.contains("include", case=False) ].sort_values(by=["EXCLUSION_REASON", "EXCEL_FILE", "UUID"], ascending=True)

logger.info(f"New dataframe with only 'include' people is \n {df}")

output_file = output_dir / "neustar_prospective_include.csv"
df.to_csv(output_file, columns=output_columns)



# none of this is needed since we want just includes in prospective
# # 2. get only the people from MKT 
# df_mkt = df [  df["EXCEL_FILE"].str.contains("mkt", case=False) ]

# logger.info(f"New Dataframe with only MKT people is \n {df_mkt}")

# # 2a. Filter "EXCLUSION_REASON = Include" for MKT people.

# df_mkt = df_mkt[ df_mkt["EXCLUSION_REASON"].str.lower() == "include" ]

# logger.info(f"Just to compare the totals {df_mkt}")


# logger.info(f"2a filtered exclude reason mkt people is \n {df}")

# # 3. get only the people from MCD

# df_mcd = df [  df["EXCEL_FILE"].str.contains("mcd", case=False) ]

# logger.info(f"New Dataframe with only MCD people is \n {df_mcd}")

# # 3a. get only EXCLUSION_REASON = Include or EXCLUSION_REASON = Include (removed from HealPros) 
# # from MCD people. 
# df_mcd_3a = df_mcd[ df_mcd["EXCLUSION_REASON"].str.contains("include", case=False) ].sort_values(by=["EXCLUSION_REASON", "UUID"], ascending=False)

# df_mcd_3a.to_csv("tmpfile.csv")

# From reviewing the dataset this is identical to just taking all includes


# df_mcd_3_include = df_mcd[ df_mcd["EXCLUSION_REASON"].str.lower() == "include" ]
# df_mcd_3_healpros = df_mcd[ df_mcd["EXCLUSION_REASON"].str.lower() == "include (removed from healpros)" ]
# df_mcd_3a = pd.concat([df_mcd_3_healpros, df_mcd_3_include]).sort_values(by=["EXCLUSION_REASON", "UUID"], ascending=False)
# df_mcd_3a.to_csv("tmpfile2.csv")
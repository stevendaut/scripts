import logging, sys

native_log_level = logging.INFO
handlers = [
    logging.StreamHandler(sys.stdout)
]
logging.basicConfig(
    level = native_log_level,
    handlers=handlers
    )
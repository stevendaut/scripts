var userDb = {
    "axel_health": {
        "password": "E5nLiEh8fCQtSsjVYNLp",
        "policy": {
            "Version": "2012-10-17",
            "Statement": [{
                    "Sid": "AllowListingOfFolder",
                    "Action": [
                        "s3:ListBucket"
                    ],
                    "Effect": "Allow",
                    "Resource": [
                        "arn:aws:s3:::" + public_bucket,
                        "arn:aws:s3:::" + public_bucket + "/axel_health/*",
                    ]
                },
                {
                    "Sid": "AllowOrdersAndResultsAccess",
                    "Effect": "Allow",
                    "Action": [
                        "s3:PutObject",
                        "s3:GetObject",
                        "s3:GetObjectVersion",
                        "s3:DeleteObject",
                    ],
                    "Resource": [
                        "arn:aws:s3:::" + public_bucket,
                        "arn:aws:s3:::" + public_bucket + "/axel_health/*",
                    ]
                },
            ]
        },
        "directoryMap": [{
                "Entry": "/axel_health",
                "Target": "/" + public_bucket + "/axel_health"
            }
        ]
    },
    "centene": {
        "password": "E5nLiEh8fCQtSsjVYNLp",
        "policy": {
            "Version": "2012-10-17",
            "Statement": [{
                    "Sid": "AllowListingOfFolder",
                    "Action": [
                        "s3:ListBucket"
                    ],
                    "Effect": "Allow",
                    "Resource": [
                        "arn:aws:s3:::" + public_bucket,
                        "arn:aws:s3:::" + public_bucket + "/centene/*",
                    ]
                },
                {
                    "Sid": "AllowOrdersAndResultsAccess",
                    "Effect": "Allow",
                    "Action": [
                        "s3:PutObject",
                        "s3:GetObject",
                        "s3:GetObjectVersion",
                        "s3:DeleteObject",
                    ],
                    "Resource": [
                        "arn:aws:s3:::" + public_bucket,
                        "arn:aws:s3:::" + public_bucket + "/centene/*",
                    ]
                },
            ]
        },
        "directoryMap": [{
                "Entry": "/centene",
                "Target": "/" + public_bucket + "/centene"
            }
        ]
    },
    "dialed_in": {
        "password": "E5nLiEh8fCQtSsjVYNLp",
        "policy": {
            "Version": "2012-10-17",
            "Statement": [{
                    "Sid": "AllowListingOfFolder",
                    "Action": [
                        "s3:ListBucket"
                    ],
                    "Effect": "Allow",
                    "Resource": [
                        "arn:aws:s3:::" + public_bucket,
                        "arn:aws:s3:::" + public_bucket + "/dialed_in/*",
                    ]
                },
                {
                    "Sid": "AllowOrdersAndResultsAccess",
                    "Effect": "Allow",
                    "Action": [
                        "s3:PutObject",
                        "s3:GetObject",
                        "s3:GetObjectVersion",
                        "s3:DeleteObject",
                    ],
                    "Resource": [
                        "arn:aws:s3:::" + public_bucket,
                        "arn:aws:s3:::" + public_bucket + "/dialed_in/*",
                    ]
                },
            ]
        },
        "directoryMap": [{
                "Entry": "/dialed_in",
                "Target": "/" + public_bucket + "/dialed_in"
            }
        ]
    },
    "unitedhealthcare": {
        "password": "E5nLiEh8fCQtSsjVYNLp",
        "policy": {
            "Version": "2012-10-17",
            "Statement": [{
                    "Sid": "AllowListingOfFolder",
                    "Action": [
                        "s3:ListBucket"
                    ],
                    "Effect": "Allow",
                    "Resource": [
                        "arn:aws:s3:::" + public_bucket,
                        "arn:aws:s3:::" + public_bucket + "/unitedhealthcare/*",
                    ]
                },
                {
                    "Sid": "AllowOrdersAndResultsAccess",
                    "Effect": "Allow",
                    "Action": [
                        "s3:PutObject",
                        "s3:GetObject",
                        "s3:GetObjectVersion",
                        "s3:DeleteObject",
                    ],
                    "Resource": [
                        "arn:aws:s3:::" + public_bucket,
                        "arn:aws:s3:::" + public_bucket + "/unitedhealthcare/*",
                    ]
                },
            ]
        },
        "directoryMap": [{
                "Entry": "/unitedhealthcare",
                "Target": "/" + public_bucket + "/unitedhealthcare"
            }
        ]
    },
    "ixLayer": {
        "password": "7sr6NK45XQyrcGFAgN5v",
        "policy": {
            "Version": "2012-10-17",
            "Statement": [{
                    "Sid": "AllowListingOfFolder",
                    "Action": [
                        "s3:ListBucket"
                    ],
                    "Effect": "Allow",
                    "Resource": [
                        "arn:aws:s3:::" + public_bucket,
                        "arn:aws:s3:::" + public_bucket + "/*",
                        "arn:aws:s3:::" + public_bucket + "/reports/*",
                    ]
                },
                {
                    "Sid": "AllowObjectAccess",
                    "Effect": "Allow",
                    "Action": [
                        "s3:PutObject",
                        "s3:GetObject",
                        "s3:DeleteObject",
                        "s3:GetObjectVersion"
                    ],
                    "Resource": [
                        "arn:aws:s3:::" + public_bucket,
                        "arn:aws:s3:::" + public_bucket + "/*",
                        "arn:aws:s3:::" + public_bucket + "/reports/*",
                    ]
                }
            ]
        },
        "directoryMap": [{
                "Entry": "/reports",
                "Target": "/" + public_bucket + "/reports"
            }
        ]
    }
}
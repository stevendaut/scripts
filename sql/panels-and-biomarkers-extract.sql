// get details on all the biomarkers for a given panel

select
    cc.text,
    cb.name,
    cb.description,
    cb.result_value_type,
    cb.expected_unit_id,
    cs.code 
from core_biomarker cb
inner join core_panel_biomarkers cpb on cb.codeableconcept_ptr_id = cpb.biomarker_id 
inner join core_panel cp on cpb.panel_id = cp.codeableconcept_ptr_id 
inner join core_codeableconcept cc on cb.codeableconcept_ptr_id = cc.id
left join core_specimentype cs on cs.id = cb.specimen_type_id
left join core_unit cu on cb.expected_unit_id = cu.id
where cp.codeableconcept_ptr_id = 79


// get all the biomarker education sections for a given panel

select cbes.title, cbes.content, cbes.display_order, cc.text as Biomarker from core_biomarkereducationsection cbes
inner join core_biomarker cb on cb.codeableconcept_ptr_id = cbes.biomarker_id 
inner join core_panel_biomarkers cpb on cb.codeableconcept_ptr_id = cpb.biomarker_id 
inner join core_panel cp on cpb.panel_id = cp.codeableconcept_ptr_id 
inner join core_codeableconcept cc on cb.codeableconcept_ptr_id = cc.id
where cp.codeableconcept_ptr_id = 79


// get all the result education sections for a given panel

select cr.icon, cr.image, cr.description, cc.text as Biomarker, (select cc.text from core_codeableconcept cc2 where cc2.id = cr.interpretation_id) as Interpretation from core_resulteducation cr
inner join core_biomarker cb on cb.codeableconcept_ptr_id = cr.biomarker_id 
inner join core_panel_biomarkers cpb on cb.codeableconcept_ptr_id = cpb.biomarker_id 
inner join core_panel cp on cpb.panel_id = cp.codeableconcept_ptr_id 
inner join core_codeableconcept cc on cb.codeableconcept_ptr_id = cc.id
where cp.codeableconcept_ptr_id = 7


// get all the codings for a given panel (note you have to have the id twice, could be improved)

select co.system, co.version, co.code, co.display, cc.text as CodeableConcept from core_coding co
inner join core_codeableconcept cc on cc.id = co.codeable_concept_id 
where co.codeable_concept_id in
(select
	cb.codeableconcept_ptr_id
from core_biomarker cb
inner join core_panel_biomarkers cpb on cb.codeableconcept_ptr_id = cpb.biomarker_id 
inner join core_panel cp on cpb.panel_id = cp.codeableconcept_ptr_id 
inner join core_codeableconcept cc on cb.codeableconcept_ptr_id = cc.id
left join core_specimentype cs on cs.id = cb.specimen_type_id
left join core_unit cu on cb.expected_unit_id = cu.id
where cp.codeableconcept_ptr_id = 79
 )
 or co.codeable_concept_id = 79

 
 
// Get all the biomarker names for a panel

select
	GROUP_CONCAT(cc.`text`) as Biomarkers 
from core_biomarker cb
inner join core_panel_biomarkers cpb on cb.codeableconcept_ptr_id = cpb.biomarker_id 
inner join core_panel cp on cpb.panel_id = cp.codeableconcept_ptr_id 
inner join core_codeableconcept cc on cb.codeableconcept_ptr_id = cc.id
left join core_specimentype cs on cs.id = cb.specimen_type_id
left join core_unit cu on cb.expected_unit_id = cu.id
where cp.codeableconcept_ptr_id = 79



// Get the biomarker IDs for a selection of panels


select
	cp.name,
	group_concat(cc.id)
from core_biomarker cb
inner join core_panel_biomarkers cpb on cb.codeableconcept_ptr_id = cpb.biomarker_id 
inner join core_panel cp on cpb.panel_id = cp.codeableconcept_ptr_id 
inner join core_codeableconcept cc on cb.codeableconcept_ptr_id = cc.id
left join core_specimentype cs on cs.id = cb.specimen_type_id
left join core_unit cu on cb.expected_unit_id = cu.id
where cp.codeableconcept_ptr_id in (79, 119, 112)
group by cp.codeableconcept_ptr_id 



// Get the units for a selection of panels

select
group_concat(expected_unit_id)
from core_biomarker cb
inner join core_panel_biomarkers cpb on cb.codeableconcept_ptr_id = cpb.biomarker_id 
inner join core_panel cp on cpb.panel_id = cp.codeableconcept_ptr_id 
inner join core_codeableconcept cc on cb.codeableconcept_ptr_id = cc.id
left join core_specimentype cs on cs.id = cb.specimen_type_id
left join core_unit cu on cb.expected_unit_id = cu.id
where cp.codeableconcept_ptr_id in (79,119, 112)


// get Codings for a selection of panels


select 
group_concat(co.id)
from core_coding co
inner join core_codeableconcept cc on cc.id = co.codeable_concept_id 
where co.codeable_concept_id in
(select
	cb.codeableconcept_ptr_id
from core_biomarker cb
inner join core_panel_biomarkers cpb on cb.codeableconcept_ptr_id = cpb.biomarker_id 
inner join core_panel cp on cpb.panel_id = cp.codeableconcept_ptr_id 
inner join core_codeableconcept cc on cb.codeableconcept_ptr_id = cc.id
left join core_specimentype cs on cs.id = cb.specimen_type_id
left join core_unit cu on cb.expected_unit_id = cu.id
where cp.codeableconcept_ptr_id in (79, 119, 112)
 )
 or co.codeable_concept_id in (79, 119, 112)

 
 // education sections
 
  
select 
group_concat(cbes.id)
from core_biomarkereducationsection cbes
inner join core_biomarker cb on cb.codeableconcept_ptr_id = cbes.biomarker_id 
inner join core_panel_biomarkers cpb on cb.codeableconcept_ptr_id = cpb.biomarker_id 
inner join core_panel cp on cpb.panel_id = cp.codeableconcept_ptr_id 
inner join core_codeableconcept cc on cb.codeableconcept_ptr_id = cc.id
where cp.codeableconcept_ptr_id in (79, 119, 112)